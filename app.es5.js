"use strict";

(function () {
    "use strict";

    var svg = document.getElementById('svg');

    function createSVGTag(tagName, attrs) {
        var SVG_NS = "http://www.w3.org/2000/svg";
        var tag = document.createElementNS(SVG_NS, tagName);

        for (var attr in attrs) {
            if (attrs.hasOwnProperty(attr)) {
                tag.setAttribute(attr, attrs[attr]);
            }
        }
        return tag;
    }

    function Circle(x, y, r, imageData) {
        this.imageData = imageData;
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = this.getColor(x, y);
        this.children = [];

        this.alive = true;
    }

    Circle.prototype.getColor = function (x, y) {
        x = Math.round(x);
        y = Math.round(y);
        var i = (y * this.imageData.width + x) * 4;
        var data = this.imageData.data;
        return 'rgba(' + [data[i], data[i + 1], data[i + 2], data[i + 3] / 255].join(',') + ')';
    };

    Circle.prototype.include = function (x, y) {
        return Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2) < this.r * this.r;
    };

    Circle.prototype.draw = function () {
        var circle = createSVGTag('circle', {
            cx: this.x,
            cy: this.y,
            r: this.r,
            fill: this.color
        });

        var animate = createSVGTag('animate', {
            repeatCount: '1', dur: '500ms', begin: 'indefinite',
            attributeName: 'r',
            from: 0,
            to: this.r
        });

        circle.appendChild(animate);
        this.element = circle;
        svg.appendChild(circle);
        animate.beginElement();
    };

    Circle.prototype.split = function () {
        this.alive = false;
        if (this.r <= 2) {
            return;
        }
        svg.removeChild(this.element);

        for (var i = 0; i < 4; i++) {
            var offsetX = this.r / 2 * (i % 2 === 1 ? -1 : 1);
            var offsetY = this.r / 2 * (i < 2 ? -1 : 1);
            var x = this.x + offsetX;
            var y = this.y + offsetY;
            var circle = new Circle(x, y, this.r / 2, this.imageData);
            circle.parent = this;
            this.children.push(circle);
            circle.draw();
        }
    };

    Circle.prototype.find = function (x, y) {
        if (this.alive) {
            if (this.include(x, y)) {
                return this;
            } else {
                return null;
            }
        } else {
            var circle = null;
            for (var i = 0, l = this.children.length; i < l; i++) {
                var child = this.children[i];
                circle = child.find(x, y);
                if (circle) {
                    return circle;
                }
            }
            return null;
        }
    };

    function Oo(radius, src, svg) {
        this.width = radius;
        this.height = radius;
        this.topCircle = null;
        this.src = src;
    }

    Oo.prototype.findCircle = function (x, y) {
        return this.topCircle.find(x, y);
    };

    Oo.prototype.loadImageData = function (src) {
        var width = this.width;
        var height = this.height;

        var img = new Image();
        img.src = src;

        return new Promise(function (resolve, reject) {
            img.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', width);

                var cxt = canvas.getContext('2d');

                cxt.drawImage(img, 0, 0, width, width);

                var imageData = cxt.getImageData(0, 0, width, width);
                resolve(imageData);
            };
            img.onerror = function (err) {
                reject(err);
            };
        });
    };

    Oo.prototype.bindEvent = function () {
        var _this = this;

        var lastX = -1000,
            lastY = -1000;

        var rect = svg.getBoundingClientRect();

        var onMove = function onMove(x, y) {
            var circle = _this.findCircle(x, y);
            if (circle && !circle.include(lastX, lastX)) {
                circle.split();
            }
            lastX = x;
            lastY = y;
        };

        svg.addEventListener("touchstart", function (event) {
            event.preventDefault();
        }, { passive: false });

        svg.addEventListener('touchmove', function (event) {
            event.preventDefault();
            var touch = event.touches[0];
            var x = touch.pageX - rect.left;
            var y = touch.pageY - rect.top;
            onMove(x, y);
        }, { passive: false });

        svg.addEventListener('mousemove', function (event) {
            var x = event.offsetX;
            var y = event.offsetY;
            onMove(x, y);
            event.preventDefault();
        });
    };

    Oo.prototype.init = function () {
        var _this2 = this;

        svg.setAttribute('width', this.width);
        svg.setAttribute('height', this.height);

        oo.loadImageData(this.src).then(function (imageData) {
            _this2.topCircle = new Circle(_this2.width / 2, _this2.height / 2, _this2.width / 2, imageData);
            _this2.topCircle.parent = _this2.topCircle;
            _this2.topCircle.draw();
            _this2.bindEvent();
        }).catch(function (error) {
            alert(error.message);
        });
    };

    var radius = Math.min(window.innerHeight, window.innerWidth, 500);

    var oo = new Oo(radius, './pic.jpg');
    oo.init();
})();
