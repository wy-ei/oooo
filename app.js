(function(){
    "use strict";
    let svg = document.getElementById('svg');

    function createSVGTag(tagName, attrs){
        var SVG_NS="http://www.w3.org/2000/svg";
        var tag = document.createElementNS(SVG_NS,tagName);

        for(let attr in attrs){
            if(attrs.hasOwnProperty(attr)) {
                tag.setAttribute(attr, attrs[attr]);
            }
        }
        return tag;
    }

    function Circle(x,y,r, imageData){
        this.imageData = imageData;
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = this.getColor(x, y);
        this.children = [];

        this.alive = true;
    }

    Circle.prototype.getColor = function(x, y){
        x = Math.round(x);
        y = Math.round(y);
        let i = (y * this.imageData.width + x) * 4;
        let data = this.imageData.data;
        return 'rgba(' + [data[i], data[i + 1], data[i + 2], data[i + 3] / 255].join(',') + ')';
    };

    Circle.prototype.include = function (x, y) {
        return (Math.pow(this.x - x ,2) + Math.pow(this.y - y ,2)) < this.r * this.r;
    };

    Circle.prototype.draw = function () {
        let circle = createSVGTag('circle', {
            cx: this.x,
            cy: this.y,
            r: this.r,
            fill: this.color
        });

        let animate = createSVGTag('animate', {
            repeatCount: '1',  dur: '500ms', begin: 'indefinite',
            attributeName: 'r',
            from: 0,
            to: this.r
        });

        circle.appendChild(animate);
        this.element = circle;
        svg.appendChild(circle);
        animate.beginElement();
    };

    Circle.prototype.split = function(){
        this.alive = false;
        if(this.r <= 2){
            return;
        }
        svg.removeChild(this.element);

        for(let i=0;i<4;i++){
            let offsetX = this.r / 2 * (i % 2 === 1 ? -1 : 1);
            let offsetY = this.r / 2 * (i < 2 ? -1 : 1);
            let x = this.x + offsetX;
            let y = this.y + offsetY;
            let circle = new Circle(x, y , this.r / 2, this.imageData);
            circle.parent = this;
            this.children.push(circle);
            circle.draw();
        }
    };

    Circle.prototype.find = function (x, y) {
        if(this.alive){
            if(this.include(x, y)){
                return this;
            }else{
                return null;
            }
        }else{
            let circle = null;
            for(let i = 0, l = this.children.length; i < l; i++){
                let child = this.children[i];
                circle = child.find(x, y);
                if(circle){
                    return circle;
                }
            }
            return null;
        }
    };

    function Oo(radius, src, svg){
        this.width = radius;
        this.height = radius;
        this.topCircle = null;
        this.src = src;
    }

    Oo.prototype.findCircle = function (x, y) {
        return this.topCircle.find(x, y);
    };

    Oo.prototype.loadImageData = function(src){
        let width = this.width;
        let height = this.height;

        let img = new Image();
        img.src = src;

        return new Promise(function(resolve, reject){
            img.onload = function(){
                let canvas = document.createElement('canvas');
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', width);

                let cxt = canvas.getContext('2d');

                cxt.drawImage(img, 0, 0, width, width);

                let imageData = cxt.getImageData(0, 0, width, width);
                resolve(imageData);
            };
            img.onerror = function(err){
                reject(err);
            }
        });
    };


    Oo.prototype.bindEvent = function(){
        let lastX = -1000,
            lastY = -1000;

        let rect = svg.getBoundingClientRect();

        let onMove = (x, y) => {
            let circle = this.findCircle(x, y);
            if(circle && !circle.include(lastX, lastX)){
                circle.split();
            }
            lastX = x;
            lastY = y;
        };

        svg.addEventListener("touchstart",(event) => {
            event.preventDefault();
        }, {passive: false});

        svg.addEventListener('touchmove', (event) => {
            event.preventDefault();
            let touch = event.touches[0];
            let x = touch.pageX - rect.left;
            let y = touch.pageY - rect.top;
            onMove(x, y);
        }, {passive: false});

        svg.addEventListener('mousemove',  (event) => {
            let x = event.offsetX;
            let y = event.offsetY;
            onMove(x, y);
            event.preventDefault();
        });


    };

    Oo.prototype.init = function(){
        svg.setAttribute('width', this.width);
        svg.setAttribute('height', this.height);

        oo.loadImageData(this.src).then((imageData) => {
            this.topCircle = new Circle(this.width / 2, this.height / 2, this.width / 2, imageData);
            this.topCircle.parent = this.topCircle;
            this.topCircle.draw();
            this.bindEvent();
        }).catch(function(error){
            alert(error.message);
        });
    };

    let radius = Math.min(window.innerHeight,  window.innerWidth, 500);

    let oo = new Oo(radius, './pic.jpg');
    oo.init();
})();
